for LINE in $(ls | grep ^lena\.[0-9]*x[0-9]*\.bmp$)
do

# valgrind --leak-check=yes --error-exitcode=1 ./tp2 -v -i c colorizar $LINE 0.5
# valgrind --leak-check=yes --error-exitcode=1 ./tp2 -v -i asm colorizar $LINE 0.5

# FILTRO="recortar"
# ./tp2 -t 10 -v -i c $FILTRO $LINE 100 > Comparativa/$FILTRO/tiempos-10x.$LINE.$FILTRO.txt
# ./tp2 -t 10 -v -i asm $FILTRO $LINE 100 >> Comparativa/$FILTRO/tiempos-10x.$LINE.$FILTRO.txt
# ./tp2 -t 100 -v -i c $FILTRO $LINE 100 > Comparativa/$FILTRO/tiempos-100x.$LINE.$FILTRO.txt
# ./tp2 -t 100 -v -i asm $FILTRO $LINE 100 >> Comparativa/$FILTRO/tiempos-100x.$LINE.$FILTRO.txt
# ./tp2 -t 1000 -v -i c $FILTRO $LINE 100 > Comparativa/$FILTRO/tiempos-1000x.$LINE.$FILTRO.txt
# ./tp2 -t 1000 -v -i asm $FILTRO $LINE 100 >> Comparativa/$FILTRO/tiempos-1000x.$LINE.$FILTRO.txt

# FILTRO="halftone"
# ./tp2 -t 10 -v -i c $FILTRO $LINE > Comparativa/$FILTRO/tiempos-10x.$LINE.$FILTRO.txt
# ./tp2 -t 10 -v -i asm $FILTRO $LINE >> Comparativa/$FILTRO/tiempos-10x.$LINE.$FILTRO.txt
# ./tp2 -t 100 -v -i c $FILTRO $LINE > Comparativa/$FILTRO/tiempos-100x.$LINE.$FILTRO.txt
# ./tp2 -t 100 -v -i asm $FILTRO $LINE >> Comparativa/$FILTRO/tiempos-100x.$LINE.$FILTRO.txt
# ./tp2 -t 1000 -v -i c $FILTRO $LINE > Comparativa/$FILTRO/tiempos.$LINE.$FILTRO.txt
# ./tp2 -t 1000 -v -i asm $FILTRO $LINE >> Comparativa/$FILTRO/tiempos.$LINE.$FILTRO.txt

FILTRO="umbralizar"
./tp2 -t 10 -v -i c $FILTRO $LINE 64 128 16 > Comparativa/$FILTRO/tiempos-10x.$LINE.$FILTRO.txt
./tp2 -t 10 -v -i asm $FILTRO $LINE 64 128 16 >> Comparativa/$FILTRO/tiempos-10x.$LINE.$FILTRO.txt
./tp2 -t 100 -v -i c $FILTRO $LINE 64 128 16 > Comparativa/$FILTRO/tiempos-100x.$LINE.$FILTRO.txt
./tp2 -t 100 -v -i asm $FILTRO $LINE 64 128 16 >> Comparativa/$FILTRO/tiempos-100x.$LINE.$FILTRO.txt
./tp2 -t 1000 -v -i c $FILTRO $LINE 64 128 16 > Comparativa/$FILTRO/tiempos-1000x.$LINE.$FILTRO.txt
./tp2 -t 1000 -v -i asm $FILTRO $LINE 64 128 16 >> Comparativa/$FILTRO/tiempos-1000x.$LINE.$FILTRO.txt

# FILTRO="colorizar"
# ./tp2 -t 10 -v -i c $FILTRO $LINE 0.5 > Comparativa/$FILTRO/tiempos-10x.$LINE.$FILTRO.txt
# ./tp2 -t 10 -v -i asm $FILTRO $LINE 0.5 >> Comparativa/$FILTRO/tiempos-10x.$LINE.$FILTRO.txt
# ./tp2 -t 100 -v -i c $FILTRO $LINE 0.5 > Comparativa/$FILTRO/tiempos-100x.$LINE.$FILTRO.txt
# ./tp2 -t 100 -v -i asm $FILTRO $LINE 0.5 >> Comparativa/$FILTRO/tiempos-100x.$LINE.$FILTRO.txt
# ./tp2 -t 1000 -v -i c $FILTRO $LINE 0.5 > Comparativa/$FILTRO/tiempos.$LINE.$FILTRO.txt
# ./tp2 -t 1000 -v -i asm $FILTRO $LINE 0.5 >> Comparativa/$FILTRO/tiempos.$LINE.$FILTRO.txt

# FILTRO="waves"
# ./tp2 -t 10 -v -i c $FILTRO $LINE 8 8 32 > Comparativa/$FILTRO/tiempos-10x.$LINE.$FILTRO.txt
# ./tp2 -t 10 -v -i asm $FILTRO $LINE 8 8 32 >> Comparativa/$FILTRO/tiempos-10x.$LINE.$FILTRO.txt
# ./tp2 -t 100 -v -i c $FILTRO $LINE 8 8 32 > Comparativa/$FILTRO/tiempos-100x.$LINE.$FILTRO.txt
# ./tp2 -t 100 -v -i asm $FILTRO $LINE 8 8 32 >> Comparativa/$FILTRO/tiempos-100x.$LINE.$FILTRO.txt
# ./tp2 -t 1000 -v -i c $FILTRO $LINE 8 8 32 > Comparativa/$FILTRO/tiempos.$LINE.$FILTRO.txt
# ./tp2 -t 1000 -v -i asm $FILTRO $LINE 8 8 32 >> Comparativa/$FILTRO/tiempos.$LINE.$FILTRO.txt

# FILTRO="rotar"
# ./tp2 -t 10 -v -i c $FILTRO $LINE > Comparativa/$FILTRO/tiempos-10x.$LINE.$FILTRO.txt
# ./tp2 -t 10 -v -i asm $FILTRO $LINE >> Comparativa/$FILTRO/tiempos-10x.$LINE.$FILTRO.txt
# ./tp2 -t 100 -v -i c $FILTRO $LINE > Comparativa/$FILTRO/tiempos-100x.$LINE.$FILTRO.txt
# ./tp2 -t 100 -v -i asm $FILTRO $LINE >> Comparativa/$FILTRO/tiempos-100x.$LINE.$FILTRO.txt
# ./tp2 -t 1000 -v -i c $FILTRO $LINE > Comparativa/$FILTRO/tiempos-1000x.$LINE.$FILTRO.txt
# ./tp2 -t 1000 -v -i asm $FILTRO $LINE >> Comparativa/$FILTRO/tiempos-1000x.$LINE.$FILTRO.txt

# valgrind --leak-check=yes --error-exitcode=1 ./tp2 -v -i c $FILTRO $LINE > Comparativa/$FILTRO/tiempos-10x.$LINE.$FILTRO.txt
# valgrind --leak-check=yes --error-exitcode=1 ./tp2 -v -i asm $FILTRO $LINE >> Comparativa/$FILTRO/tiempos-10x.$LINE.$FILTRO.txt
# valgrind --leak-check=yes --error-exitcode=1 ./tp2 -v -i c $FILTRO $LINE > Comparativa/$FILTRO/tiempos-100x.$LINE.$FILTRO.txt
# valgrind --leak-check=yes --error-exitcode=1 ./tp2 -v -i asm $FILTRO $LINE >> Comparativa/$FILTRO/tiempos-100x.$LINE.$FILTRO.txt
# valgrind --leak-check=yes --error-exitcode=1 ./tp2 -v -i c $FILTRO $LINE > Comparativa/$FILTRO/tiempos-1000x.$LINE.$FILTRO.txt
# valgrind --leak-check=yes --error-exitcode=1 ./tp2 -v -i asm $FILTRO $LINE >> Comparativa/$FILTRO/tiempos-1000x.$LINE.$FILTRO.txt

done
