; void rotar_asm (
; 	unsigned char *src,
; 	unsigned char *dst,
; 	int m,
; 	int n,
; 	int src_row_size,
; 	int dst_row_size
; );

; Parámetros:
; 	rdi = src
; 	rsi = dst
; 	rdx = m
; 	rcx = n
; 	r8 = src_row_size
; 	r9 = dst_row_size
extern printf
section .data
formatoInteger: db '%d\n', 0
raizde2sobre2: dd 0.707107
dos: dd 2.0
global rotar_asm

section .text

%define fila rdx
%define columna rcx
%define SRC rdi
%define DST rsi

rotar_asm:
        push rbp
        mov rbp, rsp
        push r12
        push r13
        push r14
        push r15

        mov edx, edx ; pongo ceros en la parte alta de rdx
        mov ecx, ecx
        mov r12, rdx
        mov r13, rcx

        ;mov rdi, formatoInteger
        ;mov rsi, r9
        ;call printf
        ;jmp .termina

        mov r12, rcx ; width
        mov r13, rdx ; height

        mov rdx,0
        mov rax, rcx
        mov rbx, 4
        div rbx
        mov r14, rax
        mov rdx, r13
        
        xor rcx,rcx

        mov eax, 0
        pinsrd xmm1, eax, 3h 

        mov eax, 1
        pinsrd xmm1, eax, 2h 

        mov eax, 2
        pinsrd xmm1, eax, 1h 

        mov eax, 3
        pinsrd xmm1, eax, 0h 
        ; XMM1 <-   |  3   |   2   |   1   |   0    |

        movdqu xmm14, [dos]

        cvtsi2ss xmm12, r12
        divss xmm12, xmm14 ; XMM2 <- n / 2 = CX
        cvttps2dq xmm12, xmm12 ; convierto a entero con truncamiento

        cvtsi2ss xmm13, r13
        divss xmm13, xmm14 ; XMM3 <- m / 2 = CY
        cvttps2dq xmm13, xmm13 ; convierto a entero con truncamiento

        cvtdq2ps xmm12, xmm12
        cvtdq2ps xmm13, xmm13

        shufps xmm12, xmm12, 0h ;   CX   |  CX  |  CX  |  CX  |
        shufps xmm13, xmm13, 0h ;   CY   |  CY  |  CY  |  CY  |
        ; CX = XMM12
        ; CY = XMM13

        movdqu xmm15, [raizde2sobre2]
        shufps xmm15, xmm15, 0h 

        mov eax, 0
        movd xmm5, eax
        shufps xmm5, xmm5, 0h ; Broadcast: |   0   |   0   |   0   |   0   |
        ; convierto xmm5 a packed single precision
        cvtdq2ps xmm5, xmm5

        movd xmm6, r13d ; XMM6 <- height
        shufps xmm6, xmm6, 0h ; broadcast
        ; convierto xmm6 a packed single precision
        cvtdq2ps xmm6, xmm6
        
        movd xmm7, r12d ; XMM7 <- width
        shufps xmm7, xmm7, 0h ; broadcast
        ; convierto xmm7 a packed single precision
        cvtdq2ps xmm7, xmm7

        xor rax, rax
        not rax
        movq xmm11, rax

        shufps xmm11,xmm11, 0b ; XMM11 debería ser todos unos
        movd xmm8, r8d
        shufps xmm8,xmm8, 0h

        
.columns:
        mov r15, rdx
        mov r9, 4
        mov eax, ecx
        mul r9d

        mov rdx, r15

        movd xmm0, eax

        shufps xmm0, xmm0, 0h  ;  X   |   X   |   X   |   X   |
        addps xmm0, xmm1 ;     X + 3  | X + 2 | X + 1 |   X   |
        ; convierto XMM0 de entero a packed single precision
        cvtdq2ps xmm0, xmm0

        mov rax, r13 ; RAX <- height
        sub rax, rdx 
        
        movd xmm2, eax
        shufps xmm2, xmm2, 0h ;   Y   |   Y   |   Y   |   Y   |
        ; convierto XMM2 de entero a packed single precision
        cvtdq2ps xmm2, xmm2

        movdqu xmm3, xmm12 
        movdqu xmm4, xmm13 

        subps xmm2, xmm4 ; y - cx
        subps xmm0, xmm3 ; x - cy
        ; XMM2 = X
        ; XMM0 = Y

        mulps xmm2, xmm15 ; RY = (x - cy) * raiz de 2 / 2 
        mulps xmm0, xmm15 ; RX = (y - cx) * raiz de 2 / 2 
        ; XMM2 = RY
        ; XMM0 = RX

        subps xmm3, xmm2 ; cx + raiz de 2 / 2 * (x - cy) - raiz de 2 / 2 * (y - cx)
        addps xmm4, xmm2 ; cy + raiz de 2 / 2 * (x - cy) + raiz de 2 / 2 * (y - cx)

        addps xmm3, xmm0 ; cx + raiz de 2 / 2 * (x - cy)
        addps xmm4, xmm0 ; cy + raiz de 2 / 2 * (x - cy)        

        ; XMM3 = U
        ; XMM4 = V
        cvttps2dq xmm3, xmm3 ; convierto a enteros packed con truncamiento
        cvttps2dq xmm4, xmm4 ; convierto a enteros packed con truncamiento

        cvtdq2ps xmm3, xmm3
        cvtdq2ps xmm4, xmm4

        movdqu xmm9, xmm3
        movdqu xmm10, xmm4
        
        cmpps xmm3, xmm7, 1b ; comparo XMM2 contra el height por menor
        cmpps xmm4, xmm6, 1b ; comparo XMM3 contra el width por menor

        andps xmm3, xmm4

        andps xmm9, xmm3 ; hago AND contra la máscara
        andps xmm10, xmm3 ; hago AND contra la máscara

        movdqu xmm3, xmm9 
        movdqu xmm4, xmm10

        cmpps xmm3, xmm5, 1b ; comparo XMM2 contra cero por menor
        cmpps xmm4, xmm5, 1b ; comparo XMM3 contra cero por menor

        orps xmm3, xmm4
        andnps xmm3, xmm11

        andps xmm9, xmm3 ; hago AND contra la máscara
        andps xmm10, xmm3 ; hago AND contra  la máscara

        cvtps2dq xmm9, xmm9 ; convierto a enteros packed con truncamiento
        cvtps2dq xmm10, xmm10 ; convierto a enteros packed con truncamiento        
        
        ; hago los cálculos para indexar la imagen fuente (4 índices)


        pmulld xmm10, xmm8 ; xmm8 deberia ser entero packed
        paddd xmm9, xmm10

        movd r9d, xmm9 
        xor rbx, rbx
        xor rax, rax

        cmp r9, 0
        je .pixel1 
        
        mov al, [SRC + r9]
.pixel1:
        shl rax, 8
        psrldq xmm9, 4
        movd r9d, xmm9
        cmp r9, 0
        je .pixel2
        mov al, [SRC + r9]
.pixel2:
        shl rax, 8
        psrldq xmm9, 4
        movd r9d, xmm9
        cmp r9, 0
        je .pixel3
        mov al, [SRC + r9]
.pixel3:
        shl rax, 8
        psrldq xmm9, 4
        movd r9d, xmm9
        cmp r9, 0
        je .continuo
        mov al, [SRC + r9]
.continuo:
        mov [DST], eax
        add DST, 4
        
        inc rcx
        cmp rcx, r14
        jl .columns
.rows:
        xor rcx, rcx
        dec rdx
        jnz .columns
.termina:
        pop r15
        pop r14
        pop r13
        pop r12
        pop rbp
        ret
