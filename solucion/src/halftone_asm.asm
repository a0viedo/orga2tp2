; void halftone_asm (
; 	unsigned char *src,
; 	unsigned char *dst,
; 	int m,
; 	int n,
; 	int src_row_size,
; 	int dst_row_size
; );

; Parámetros:
; 	rdi = src
; 	rsi = dst
; 	edx = m
; 	ecx = n
; 	r8d = src_row_size
; 	r9d = dst_row_size


global halftone_asm

section .text

halftone_asm:	
	shr edx, 1
	shr ecx, 4
	mov r8d, r8d			;Limpio la parte superior de r8
	mov r10d, ecx			;Resguardo mi contador horizontal
	mov r11d,r9d			;Resguardo el ancho de la imagen destino
	
	mov r9, 0x03340334		;Preparar máscara con 8x820
	movq xmm10, r9
	pshufd xmm10, xmm10, 00000000b
	mov r9, 0x02670267		;Preparar máscara con 8x615
	movq xmm11, r9
	pshufd xmm11, xmm11, 00000000b
	mov r9, 0x019A019A		;Preparar máscara con 8x410
	movq xmm12, r9
	pshufd xmm12, xmm12, 00000000b
	mov r9, 0x00CD00CD		;Preparar máscara con 8x205
	movq xmm13, r9
	pshufd xmm13, xmm13, 00000000b
	
	mov r9, 0xFFFFFFFF
	movq xmm14, r9
	pshufd xmm14, xmm14, 00000000b
	pxor xmm15, xmm15

	.ciclo_row:
	mov ecx, r10d
	mov al, 1			;Seteo al para ser distinto a cero.

	.ciclo_col:
	movdqu xmm0, [rdi]		;Cargo 16 pixels de la fila actual.
	movdqu xmm1, [rdi+r8]		;Ahora tengo 8 bloques de 2x2.
	movdqu xmm2, xmm0
	movdqu xmm3, xmm1
	punpcklbw xmm0, xmm15		;Extiendo todo a words para hacer cálculos.
	punpckhbw xmm2, xmm15
	punpcklbw xmm1, xmm15
	punpckhbw xmm3, xmm15
	paddw xmm0, xmm1		;Sumo los pixels de cada bloque.
	paddw xmm2, xmm3
	phaddw xmm0, xmm2		;Empaqueto los resultados y ahora tengo las 8 sumas en xmm0.
	;Comparar packed con 820, guardo resultado en registro temporal para upper y otro lower.
	movdqu xmm5, xmm10
	pcmpgtw xmm5, xmm0		;Obtengo máscara (820 > suma?)
	pxor xmm5, xmm14		;La invierto
	movdqu xmm2, xmm5		;Guardo en upper mask.
	movdqu xmm3, xmm5		;Lower mask
	;Comparar packed con 615, hago un OR con la lower y luego un shift y a eso un OR con upper.
	movdqu xmm5, xmm11
	pcmpgtw xmm5, xmm0		;Obtengo máscara (615 > suma?)
	pxor xmm5, xmm14		;La invierto
	por xmm3, xmm5			;Lower
	psrlw xmm5, 8
	por xmm2, xmm5			;Upper
	;Comparar packed con 410, copio, hago shift y OR con lower, shift de la copia y OR con upper.
	movdqu xmm5, xmm12
	pcmpgtw xmm5, xmm0		;Obtengo máscara (410 > suma?)
	pxor xmm5, xmm14		;La invierto
	movdqu xmm6, xmm5
	psrlw xmm5, 8
	por xmm2, xmm5			;Upper
	psllw xmm6, 8
	por xmm3, xmm6			;Lower
	;Comparar packed con 205, shift y OR con upper
	movdqu xmm5, xmm13
	pcmpgtw xmm5, xmm0		;Obtengo máscara (205 > suma?)
	pxor xmm5, xmm14		;La invierto
	psrlw xmm5, 8
	por xmm2, xmm5			;Upper
	
	movdqu [rsi], xmm2
	movdqu [rsi+r11], xmm3
	
	add rdi, 16
	add rsi, 16
	dec ecx
	jnz .ciclo_col
	
	cmp al, 0
	je .sigue_ciclo			;Si ya procesé las dos filas enteras, sigo con las próximas.

	mov rax, r10
	shl rax, 4			;Obtengo la cantidad de pixels que recorrí.
	mov r9, r11
	sub r9, rax			;Obtengo la cantidad de pixels que me falta recorrer.
	cmp r9, 0
	je .sigue_ciclo			;Si son cero, paso a las filas siguientes.
	add rdi, r9
	sub rdi, 16			;Me posiciono para poder procesar los últimos pixels
	add rsi, r9
	sub rsi, 16
	inc ecx
	xor al, al			;Uso al en cero para indicar que ya procesé los últimos pixels
	jmp .ciclo_col			;Proceso los últimos 16 pixels.
	
	.sigue_ciclo:
	mov r9, r8
	sub r9, r11			;Obtengo la diferencia entre el ancho del origen y el destino.
	add rdi, r9
	add rdi, r8			;Ajusto los punteros para las siguientes filas.
	add rsi, r11
	dec edx
	jnz .ciclo_row

	ret
