#include <math.h>

#define MIN(a, b) (((a) < (b)) ? (a) : (b))
#define MAX(a, b) (((a) > (b)) ? (a) : (b))

float sin_taylor (float x) {
	const float pi = 3.14159265359;
	float r, y;
	int k = floor(x / (2 * pi));
	
	r = x - (2 * pi * k);
	x = r - pi;
	y = x - (pow(x, 3) / 6) + (pow(x, 5) / 120) - (pow(x, 7) / 5040);
	
	return y;
}

void waves_c (
	unsigned char *src,
	unsigned char *dst,
	int m,
	int n,
	int row_size,
	float x_scale,
	float y_scale,
	float g_scale
) {
	unsigned char (*src_matrix)[row_size] = (unsigned char (*)[row_size]) src;
	unsigned char (*dst_matrix)[row_size] = (unsigned char (*)[row_size]) dst;

	int i = 0, j;
	while(i < m) {
		j = 0;
		while(j < n) {
			float prof_ij = (x_scale * sin_taylor(i/8.0)) + (y_scale * sin_taylor(j/8.0));
			dst_matrix[i][j] = MAX(MIN(floor((prof_ij * g_scale) / 2) + src_matrix[i][j], 255), 0);
			j++;
		}
		i++;
	}

}
