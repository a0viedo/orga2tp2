void rotar_c (
	unsigned char *src,
	unsigned char *dst,
	int m,
	int n,
	int src_row_size,
	int dst_row_size
) {
	unsigned char (*src_matrix)[src_row_size] = (unsigned char (*)[src_row_size]) src;
	unsigned char (*dst_matrix)[dst_row_size] = (unsigned char (*)[dst_row_size]) dst;
        int u, v, cx, cy;
        
        cy = (int) (m/2);
        cx = (int) (n/2);
        float raiz = sqrt(2.0) / 2.0;
        
        for(int x = 0; x < m; x++) 
        {
            
            for(int y = 0; y < n; y++)
            {
                // evaluo u y v
                float rx = raiz*(y - cx);
                float ry = raiz*(x - cy);
                u = (int) (rx - ry + cx);
                v = (int) (rx + ry + cy);
                if(0 <= u && u < n && 0 <= v && v < m)
                {
                    dst_matrix[x][y] = src_matrix[v][u];
                }
                else 
                {
                    dst_matrix[x][y] = 0;
                }
            }
            
        }
}
