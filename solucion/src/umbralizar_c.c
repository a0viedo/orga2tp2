#include <math.h>

void umbralizar_c (
	unsigned char *src,
	unsigned char *dst,
	int m,
	int n,
	int row_size,
	unsigned char min,
	unsigned char max,
	unsigned char q
) {
	unsigned char (*src_matrix)[row_size] = (unsigned char (*)[row_size]) src;
	unsigned char (*dst_matrix)[row_size] = (unsigned char (*)[row_size]) dst;

	// TODO: Implementar
        for(int x = 0; x < m; x++) 
        {
            for(int y = 0; y < n; y++)
            {
                if(src_matrix[x][y] < min)
                {
                    dst_matrix[x][y] = 0;
                }
                else if(src_matrix[x][y] > max)
                {
                    dst_matrix[x][y] = 255;
                }
                else 
                {
                    dst_matrix[x][y] = floor(src_matrix[x][y] / q) * q;
                }
            }
        }
}
