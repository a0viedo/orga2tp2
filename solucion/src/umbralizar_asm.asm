; void umbralizar_c (
; 	unsigned char *src,
; 	unsigned char *dst,
; 	int m,
; 	int n,
; 	int row_size,
; 	unsigned char min,
; 	unsigned char max,
; 	unsigned char q
; );

; Parámetros:
; 	rdi = src
; 	rsi = dst
; 	rdx = m
; 	rcx = n
; 	r8 = row_size
; 	r9 = min
; 	rbp + 16 = max
; 	rbp + 24 = q

section.data
cerosEnParteAlta: DB 0xFF, 0x0, 0xFF, 0x0, 0xFF, 0x0, 0xFF, 0x0, 0xFF, 0x0, 0xFF, 0x0, 0xFF, 0x0, 0xFF, 0x0

global umbralizar_asm

section .text

umbralizar_asm:
	push rbp
	mov rbp, rsp
	push r12

	pxor xmm0, xmm0 ; Lo voy a usar para cuando tenga que llenar cosas con ceros, o para unpackear bytes a words con ceros intercalados

	movq xmm9, r9 ; xmm9 = 0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|min
	pshuflw xmm9, xmm9, 0 ; xmm9 = 0|0|...|0|min|0|min
	pshufd xmm9, xmm9, 0 ; xmm9 = 0|min|...|0|min

	movzx r9, byte [rbp + 16]
	movq xmm10, r9 ; xmm10 = 0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|max
	pshuflw xmm10, xmm10, 0 ; xmm10 = 0|0|...|0|max|0|max
	pshufd xmm10, xmm10, 0 ; xmm10 = 0|max|...|0|max

	movzx r9, byte [rbp + 24]
	movq xmm11, r9 ; xmm11 = 0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|q
	pshuflw xmm11, xmm11, 0 ; xmm11 = 0|0|...|0|q|0|q
	pshufd xmm11, xmm11, 0 ; xmm11 = 0|q|...|0|q	
	punpcklwd xmm11, xmm0 
	movdqu xmm12, xmm11 ; En xmm11 lo guardo como unsigned integer dwords para multiplicar
	cvtdq2ps xmm12, xmm12 ; En xmm12 lo voy a guardar como float para dividir

	mov eax, edx
	mul r8d
	mov r9d, eax ; En r9d me guardo filas X row_size

	xor rax, rax ; Voy a usar rax para apuntar al principio de la fila actual
	.inicioLoopFila:
	mov r10, rax ; Voy a usar r10 para guardar el indice del primer elemento que estoy viendo, es decir (fila * row_size) + columna
	xor r11, r11 ; Voy a usar r11 para el contador de indice de la columna, asi veo de no pasarme

	.inicioLoopColumna:
	movq xmm1, [rdi + r10] ; En xmm1 tengo los 8 bytes que voy a procesar
	punpcklbw xmm1, xmm0 ; xmm1 = 0|a7|...|0|a0

	movdqu xmm2, xmm1
	pcmpgtw xmm2, xmm9 ; xmm2 = Mascara que me dice cuales de los pixels son mayores que min
	pcmpeqw xmm2, xmm0 ; xmm2 = Mascara que me dice cuales de los pixels son menores o iguales que min

	movdqu xmm3, xmm1
	pcmpeqw xmm3, xmm9 ; xmm3 = Mascara que me dice cuales de los pixels son iguales que min

	pxor xmm3, xmm2 ; xmm3 = Mascara que me dice cuales de los pixels son menores que min

	movdqu xmm4, xmm1
	pcmpgtw xmm4, xmm10 ; xmm2 4 Mascara que me dice cuales de los pixels son mayores que max

	movdqu xmm5, xmm4

	por xmm5, xmm3 ; xmm5 = Mascara que me dice cuales de los pixels no estan entre min y max
	pcmpeqw xmm5, xmm0 ;xmm5 = Mascara que me dice cuales de los pixels estan entre min y max

	pand xmm5, xmm1 ; xmm5 = valor original en los que estan entre min y max, 0 en el resto

	movdqu xmm6, xmm5 ; Lo copio a xmm6
	movdqu xmm7, xmm5 ; Lo copio a xmm7

	punpcklwd xmm6, xmm0 ; xmm6 tiene la parte baja de xmm5, como dwords extendidas con ceros
	punpckhwd xmm7, xmm0 ; xmm7 tiene la parte alta de xmm5, como dwords extendidas con ceros

	cvtdq2ps xmm6, xmm6 ; Lo paso a float para dividir
	cvtdq2ps xmm7, xmm7 ; Lo paso a float para dividir

	divps xmm6, xmm12 ; Divido cada float por Q
	divps xmm7, xmm12 ; Divido cada float por Q

	cvttps2dq xmm6, xmm6 ; Convierto el resultado de la division en int dword con truncamiento
	cvttps2dq xmm7, xmm7 ; Convierto el resultado de la division en int dword con truncamiento
	
	pmullw xmm6, xmm11 ; Multiplico por q cada words 
	pmullw xmm7, xmm11 ; Multiplico por q cada word

	packssdw xmm6, xmm7 ; Junto de vuelta los dwords a words 

	pand xmm4, [cerosEnParteAlta]
	paddusb xmm6, xmm4 ; Pongo en 255 los que son mayores que max

	packuswb xmm6, xmm0 ; Enpaqueto a byte

	movq [rsi + r10], xmm6

	add r10d, 8 ; Incremento la posicion actual
	add r11d, 8

	cmp r11d, ecx
	je .finLoopColumna ; Si r11d = m, se acabo la fila
	jl .inicioLoopColumna ; Si r11d es menor que m, sigo iterando

	mov r12, r11 ; Si me pasé, significa que m no es multiplo de 8, y tengo que retroceder para procesar los ultimos bytes
	sub r12d, ecx ; calculo lo que me pase

	sub r11d, r12d ; Le resto al contador de la fila lo que me pase
	sub r11d, 8 ; y 8 para terminar en el proximo paso

	sub r10d, r12d ; Le resto a la posicion actual lo que me pase
	sub r10d, 8 ; y 8 para terminar en el proximo paso

	jmp .inicioLoopColumna

	.finLoopColumna:

	add eax, r8d
	cmp eax, r9d
	jl .inicioLoopFila

	pop r12
	pop rbp
	ret

