void recortar_c (
	unsigned char *src,
	unsigned char *dst,
	int m,
	int n,
	int src_row_size,
	int dst_row_size,
	int tam
) {
	unsigned char (*src_matrix)[src_row_size] = (unsigned char (*)[src_row_size]) src;
	unsigned char (*dst_matrix)[dst_row_size] = (unsigned char (*)[dst_row_size]) dst;

        for(int y = 0; y < tam; y++)             // x es el vector columna
        {                                        // y es el vector fila
            for(int x = 0; x < tam; x++)
            {
                dst_matrix[tam + x][tam + y] = src_matrix[x][y];
            }
        }
        for(int y = n-tam; y < n; y++)
        {
            for(int x = m-tam; x < m; x++)
            {
                dst_matrix[x - (m-tam)][y - (n-tam)] = src_matrix[x][y];
            }
        }
        for(int y = n - tam; y < n; y++)
        {
            for(int x = 0; x < tam; x++)
            {
                dst_matrix[x + tam][y - (n - tam)] = src_matrix[x][y];
            }
        }
        for(int y = 0; y < tam; y++)
        {
            for(int x = m - tam; x < m; x++)
            {
                dst_matrix[x - (m-tam)][y + tam] = src_matrix[x][y];
            }
        }
	// TODO: Implementar
        
}
