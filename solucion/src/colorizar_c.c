#include<math.h>
void colorizar_c (
	unsigned char *src,
	unsigned char *dst,
	int m,
	int n,
	int src_row_size,
	int dst_row_size,
	float alpha
) {
	unsigned char (*src_matrix)[src_row_size] = (unsigned char (*)[src_row_size]) src;
	unsigned char (*dst_matrix)[dst_row_size] = (unsigned char (*)[dst_row_size]) dst;
	
	for (int i = 1; i < m - 1; i++)
	{
	    int limit = (n *3) - 3;
	    for(int j = 3; j < limit ; j+=3)
	    {
		unsigned char maxB = src_matrix[i][j + 0]; //src(i, j)B

		if(src_matrix[i - 1][j + 0 - 3] > maxB) //src(i-1, j-1)B
		{
		    maxB = src_matrix[i - 1][j + 0 - 3];
		}
		if(src_matrix[i - 1][j + 0] > maxB) //src(i-1, j)B
		{
		    maxB = src_matrix[i - 1][j + 0];
		}
		if(src_matrix[i - 1][j + 0 + 3] > maxB) //src(i-1, j+1)B
		{
		    maxB = src_matrix[i - 1][j + 0 + 3];
		}
		if(src_matrix[i][j + 0 - 3] > maxB) //src(i, j-1)B
		{
		    maxB = src_matrix[i][j + 0 - 3];
		}
		if(src_matrix[i][j + 0 + 3] > maxB) //src(i, j+1)B
		{
		    maxB = src_matrix[i][j + 0 + 3];
		}
		if(src_matrix[i + 1][j + 0 - 3] > maxB) //src(i+1, j-1)B
		{
		    maxB = src_matrix[i + 1][j + 0 - 3];
		}
		if(src_matrix[i + 1][j + 0] > maxB) //src(i+1, j)B
		{
		    maxB = src_matrix[i + 1][j + 0];
		}
		if(src_matrix[i + 1][j + 0 + 3] > maxB) //src(i+1, j+1)B
		{
		    maxB = src_matrix[i + 1][j + 0 + 3];
		}

		unsigned char maxG = src_matrix[i][j + 1]; //src(i, j)G
		if(src_matrix[i - 1][j + 1 - 3] > maxG) //src(i-1, j-1)G
		{
		    maxG = src_matrix[i - 1][j + 1 - 3];
		}
		if(src_matrix[i - 1][j + 1] > maxG) //src(i-1, j)G
		{
		    maxG = src_matrix[i - 1][j + 1];
		}
		if(src_matrix[i - 1][j + 1 + 3] > maxG) //src(i-1, j+1)G
		{
		    maxG = src_matrix[i - 1][j + 1 + 3];
		}
		if(src_matrix[i][j + 1 - 3] > maxG) //src(i, j-1)G
		{
		    maxG = src_matrix[i][j + 1 - 3];
		}
		if(src_matrix[i][j + 1 + 3] > maxG) //src(i, j+1)G
		{
		    maxG = src_matrix[i][j + 1 + 3];
		}
		if(src_matrix[i + 1][j + 1 - 3] > maxG) //src(i+1, j-1)G
		{
		    maxG = src_matrix[i + 1][j + 1 - 3];
		}
		if(src_matrix[i + 1][j + 1] > maxG) //src(i+1, j)G
		{
		    maxG = src_matrix[i + 1][j + 1];
		}
		if(src_matrix[i + 1][j + 1 + 3] > maxG) //src(i+1, j+1)G
		{
		    maxG = src_matrix[i + 1][j + 1 + 3];
		}

		unsigned char maxR = src_matrix[i][j + 2]; //src(i, j)R
		if(src_matrix[i - 1][j + 2 - 3] > maxR) //src(i-1, j-1)R
		{
		    maxR = src_matrix[i - 1][j + 2 - 3];
		}
		if(src_matrix[i - 1][j + 2] > maxR) //src(i-1, j)R
		{
		    maxR = src_matrix[i - 1][j + 2];
		}
		if(src_matrix[i - 1][j + 2 + 3] > maxR) //src(i-1, j+1)R
		{
		    maxR = src_matrix[i - 1][j + 2 + 3];
		}
		if(src_matrix[i][j + 2 - 3] > maxR) //src(i, j-1)R
		{
		    maxR = src_matrix[i][j + 2 - 3];
		}
		if(src_matrix[i][j + 2 + 3] > maxR) //src(i, j+1)R
		{
		    maxR = src_matrix[i][j + 2 + 3];
		}
		if(src_matrix[i + 1][j + 2 - 3] > maxR) //src(i+1, j-1)R
		{
		    maxR = src_matrix[i + 1][j + 2 - 3];
		}
		if(src_matrix[i + 1][j + 2] > maxR) //src(i+1, j)R
		{
		    maxR = src_matrix[i + 1][j + 2];
		}
		if(src_matrix[i + 1][j + 2 + 3] > maxR) //src(i+1, j+1)R
		{
		    maxR = src_matrix[i + 1][j + 2 + 3];
		}

		float titaB = maxB > maxG && maxB > maxR ? 1 + alpha : 1 - alpha;
		float titaG = maxG > maxR && maxG >= maxB ? 1 + alpha : 1 - alpha;
		float titaR = maxR >= maxB && maxR >= maxG ? 1 + alpha : 1 - alpha;

		float newFB = titaB * src_matrix[i][j + 0];
		float newFG = titaG * src_matrix[i][j + 1];
		float newFR = titaR * src_matrix[i][j + 2];

		unsigned int newB = floor(newFB);
		unsigned int newG = floor(newFG);
		unsigned int newR = floor(newFR);

		if(newB > 255)
		{
		    dst_matrix[i][j + 0] = 255;
		}
		else
		{
		    dst_matrix[i][j + 0] = newB;
		}

		if(newG > 255)
		{
		    dst_matrix[i][j + 1] = 255;
		}
		else
		{
		    dst_matrix[i][j + 1] = newG;
		}

		if(newR > 255)
		{
		    dst_matrix[i][j + 2] = 255;
		}
		else
		{
		    dst_matrix[i][j + 2] = newR;
		}
	    }
	}
}
