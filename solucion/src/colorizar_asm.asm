; void colorizar_asm (
; 	unsigned char *src,
; 	unsigned char *dst,
; 	int m,
; 	int n,
; 	int src_row_size,
; 	int dst_row_size,
;   float alpha
; );

; Parámetros:
; 	rdi = src
; 	rsi = dst
; 	rdx = m
; 	rcx = n
; 	r8 = src_row_size
; 	r9 = dst_row_size
;   xmm0 = alpha


global colorizar_asm
section .data
unoFloat: DD 1.0
rotarPixelsAIzquierda: DQ 0x0A09080706050403, 0x808080800E0D0C0B
rotarColoresAIzquierda: DQ 0x0807030504000201, 0x8080808080808006
pixelsAzules: DQ 0x00FF0000FF0000FF, 0x0
pixelsVerdes: DQ 0xFF0000FF0000FF00, 0x0
pixelsRojos:  DQ 0x0000FF0000FF0000, 0xFF

section .text
colorizar_asm:
	push rbp
	mov rbp, rsp
	push r12
	push r13
	push r14
	push r15
	
	sub rdx, 2 ; Decremento m, ya que quiero evaluar hasta la fila m - 1
	mov r14, 0 ; r14 = j, indice para no pasarme de las cantidad filas
	
	mov r15, rdx
	mov eax, ecx
	mov r10, 3
	mul r10d
	sub eax, 7
	xor rdx, rdx
	mov r11, 9
	div r11
	mov r11, rax ; En r11d me guardo ((n x 3) - 7) / 9	
	mov r12, rdx
	add r12, 7 ; Resto de la dicision de arriba, + 7, que es lo que tengo que avanzar en la ultima iteración
	mov rdx, r15
	
	xor rax, rax ; Voy a usar rax para apuntar al primer elemento de la fila anterior a la que esta el pixel a colorizar_asm	
	
	movdqu xmm10, [rotarPixelsAIzquierda] 
	movdqu xmm11, [rotarColoresAIzquierda]
	pxor xmm12, xmm12 ; Mascara con ceros
	movdqu xmm13, [unoFloat] 
	movdqu xmm14, xmm13
	subss xmm13, xmm0 
	addss xmm14, xmm0 
	shufps xmm13, xmm13, 0h ; 1 - alpha replicado como single precision float
	shufps xmm14, xmm14, 0h ; 1 + alpha replicado como single precision float
	
	.inicioLoopFila:
	mov r10, rax ; r10 = principio de la fila anterior
	
	mov r13, 0 ; Es el indice en la fila que estoy viendo	
	
	xor r15, r15
	
	.inicioLoopColumna:
	movdqu xmm0, [rdi + r10]; En xmm0 tengo 16 bytes de la fila anterior a la que voy a procesar
	add r10, r8
	movdqu xmm1, [rdi + r10] ; En xmm1 tengo 16 bytes de la fila a procesar
	movdqu xmm15, xmm1 ; Me lo guardo para usarlo al final
	add r10, r8
	movdqu xmm2, [rdi + r10] ; En xmm2 tengo 16 bytes de la fila siguiente a la que voy a procesar
	sub r10, r8
	
	.calculoMaximos:
	pmaxub xmm0, xmm1 ; xmm0 = maximo b|g|r entre la fila j y la j-1, byte a byte
	pmaxub xmm0, xmm2 ; xmm0 = maximo b|g|r entre la fila j, la j-1 y la j + 1, byte a byte

	; en cada iteracion quiero procesar los pixels 2, 3 y 4 que me traigo, asi que roto para que me queden alineados
	movdqu xmm1, xmm0 ; xmm0 = max px1-5 b1,g1,r1|b2,g2,r2|b3,g3,r3|b4,g4,r4|b5,g5,r5|byteFruta
	pshufb xmm1, xmm10 ; xmm1 = max px1-5 b2,g2,r2|b3,g3,r3|b4,g4,r4|b5,g5,r5|0..0
	movdqu xmm2, xmm1 ; xmm2 = max px1-5 b3,g3,r3|b4,g4,r4|b5,g5,r5|0..0
	pshufb xmm2, xmm10
	
	pmaxub xmm0, xmm1 
	pmaxub xmm0, xmm2 ; xmm0 = max bPixel1,gPixel1,rPixel1|bPixel2,gPixel2,rPixel2|bPixel3,gPixel3,rPixel3|..(posibles peras, bananas y manzanas, por el byteFruta)

	movdqu xmm1, xmm0 ; Roto de a colores para que me queden alineados
	pshufb xmm1, xmm11 ; xmm0 = b1,g1,r1|b2,g2,r2|b3,g3,r3|fruta, fruta Everywhere
	movdqu xmm2, xmm1  ; xmm1 = g1,r1,b1|g2,r2,b2|g3,r3,b3|0..0
	pshufb xmm2, xmm11 ; xmm2 = r1,b1,g1|r2,b2,g2|r3,b3,g3|0..0
	
	.mascaraAzules:
	movdqu xmm3, xmm0 
	movdqu xmm9, [pixelsAzules]
	pand xmm3, xmm9 ;xmm3 = b1,0,0|b2,0,0|b3,0,0|0..0
	movdqu xmm4, xmm3
	psubusb xmm4, xmm1 ; xmm4 = b1-g1,x,x|b2-g2,x,x|b3-g3,x,x|x..x cada x es un numero mayor o igual a cero, que no me interesa
	pcmpeqb xmm4, xmm12 ; xmm4 = mascara con ceros en donde bn > gn 
	pcmpeqb xmm4, xmm12 ; xmm4 = invierto la mascara anterior
	psubusb xmm3, xmm2 ; xmm3 = b1-r1,x,x|b2-r2,x,x|b3-r3,x,x|x..x cada x es un numero mayor o igual a cero, que no me interesa
	pcmpeqb xmm3, xmm12 ; xmm3 = mascara con ceros en donde bn > rn 
	pcmpeqb xmm3, xmm12 ; xmm3 = invierto la mascara anterior
	pand xmm3, xmm4 ;  xmm3 = mascara final, con unos donde bn > gn y bn > rn
	
	.mascaraVerdes:
	movdqu xmm4, xmm0 
	movdqu xmm9, [pixelsVerdes]
	pand xmm4, xmm9 ;xmm4 = 0,g1,0|0,g2,0|0,g3,0|0..0
	movdqu xmm6, xmm4
	movdqu xmm5, xmm4
	psubusb xmm6, xmm1 ; xmm6 = x,g1-r1,x|x,g2-r2,x|x,g3-r3,x|x..x cada x es un numero mayor o igual a cero, que no me interesa
	pcmpeqb xmm6, xmm12 ; xmm6 = mascara con ceros en donde gn > rn 
	pcmpeqb xmm6, xmm12 ; xmm6 = invierto la mascara anterior
	psubusb xmm5, xmm2 ; xmm5 = x,g1-b1,x|x,g2-b2,x|x,g3-b3,x|x..x cada x es un numero mayor o igual a cero, que no me interesa
	pcmpeqb xmm5, xmm12 ; xmm5 = mascara con ceros en donde gn > bn 
	pcmpeqb xmm5, xmm12 ; xmm5 = invierto la mascara anterior
	pcmpeqb xmm4, xmm2 ; xmm4 = mascara con unos donde gn = bn
	por xmm4, xmm5 ; xmm4 = mascara con unos donde gn>=bn
	pand xmm4, xmm6 ;  xmm4 = mascara final, con unos donde gn >= bn y gn > rn
	pand xmm4, xmm9 ; Me quedo solo con los pixels verdes, borro el resto
	
	.mascaraRojos:
	movdqu xmm5, xmm0 
	movdqu xmm9, [pixelsRojos]
	pand xmm5, xmm9 ;xmm5 = 0,0,r1|0,0,r2|0,0,r3|0..0
	movdqu xmm8, xmm5
	movdqu xmm7, xmm5
	movdqu xmm6, xmm5
	psubusb xmm8, xmm1 ; xmm8 = x,x,r1-b1|x,x,r2-b2|x,x,r3-b3|x..x cada x es un numero mayor o igual a cero, que no me interesa
	pcmpeqb xmm8, xmm12 ; xmm8 = mascara con ceros en donde rn > bn 
	pcmpeqb xmm8, xmm12 ; xmm8 = invierto la mascara anterior
	pcmpeqb xmm7, xmm1 ; xmm7 = mascara con unos donde rn = bn
	por xmm8, xmm7 ; xmm8 = mascara con unos donde rn >= bn
	psubusb xmm6, xmm2 ; xmm6  = x,x,r1-g1|x,x,r2-g2|x,x,r3-g3|x..x cada x es un numero mayor o igual a cero, que no me interesa
	pcmpeqb xmm6, xmm12 ; xmm6 = mascara con ceros en donde rn > gn 
	pcmpeqb xmm6, xmm12 ; xmm6 = invierto la mascara anterior
	pcmpeqb xmm5, xmm2 ; xmm5 = mascara con unos donde rn = gn
	por xmm5, xmm6 ; xmm5 = mascara con unos donde rn>=gn
	pand xmm5, xmm8 ;  xmm5 = mascara final, con unos donde rn >= bn y rn >= gn
	pand xmm5, xmm9 ; Me quedo solo con los pixels rojos, borro el resto
	
	paddb xmm3, xmm4 ; uno las tres mascaras en xmm3
	paddb xmm3, xmm5; xmm3 = mascara de bytes a intensificar
	movdqu xmm4, xmm3  
	pcmpeqb xmm4, xmm12 ; xmm4 = mascara de bytes a atenuar
	
	pshufb xmm15, xmm10 ; En mis dos mascaras, yo tengo px2|px3|px4|0..0, asi que agarro los pixels de src y los roto para que me queden igual
	
	pand xmm3, xmm15 ; Me quedo con el valor original de los pixels a intensificar
	pand xmm4, xmm15 ; Me quedo con el valor original de los pixels a atenuar
	
	.intensificar:
	movdqu xmm5, xmm3
	
	movdqu xmm7, xmm5 ; Guardo los valores para separar parte baja y alta
	punpcklbw xmm5, xmm12 ; Baja
	punpckhbw xmm7, xmm12 ; Alta
	
	movdqu xmm6, xmm5 ; Guardo los valores para separar parte baja y alta
	punpcklwd xmm5, xmm12 ; Baja
	punpckhwd xmm6, xmm12 ; Alta
	
	movdqu xmm8, xmm7 ; Guardo los valores para separar parte baja y alta
	punpcklwd xmm7, xmm12 ; Baja
	punpckhwd xmm8, xmm12 ; Alta
	
	; Me queda xmm3 =  xmm5|xmm6|xmm7|xmm8, todos desempaquetados a dw

	cvtdq2ps xmm5, xmm5 ; Los paso a float
	cvtdq2ps xmm6, xmm6
	cvtdq2ps xmm7, xmm7
	cvtdq2ps xmm8, xmm8

	mulps xmm5, xmm14 ; Multiplico a todos por 1+alpha
	mulps xmm6, xmm14
	mulps xmm7, xmm14
	mulps xmm8, xmm14

	cvttps2dq xmm5, xmm5 ; Vuelvo todos a dword int con truncamiento
	cvttps2dq xmm6, xmm6
	cvttps2dq xmm7, xmm7
	cvttps2dq xmm8, xmm8

	packssdw xmm5, xmm6 ; Empaqueto todos con saturacion con signo
	packssdw xmm7, xmm8 ; Primero las cuatro dwords a words
	packuswb xmm5, xmm7 ; Y aca de words a bytes, saturando sin signo
	
	movdqu xmm3, xmm5
	
	.atenuar:
	movdqu xmm5, xmm4
	
	movdqu xmm7, xmm5 ; Guardo los valores para separar parte baja y alta
	punpcklbw xmm5, xmm12 ; Baja
	punpckhbw xmm7, xmm12 ; Alta
	
	movdqu xmm6, xmm5 ; Guardo los valores para separar parte baja y alta
	punpcklwd xmm5, xmm12 ; Baja
	punpckhwd xmm6, xmm12 ; Alta
	
	movdqu xmm8, xmm7 ; Guardo los valores para separar parte baja y alta
	punpcklwd xmm7, xmm12 ; Baja
	punpckhwd xmm8, xmm12 ; Alta
	
	; Me queda xmm4 =  xmm5|xmm6|xmm7|xmm8, todos desempaquetados a dw

	cvtdq2ps xmm5, xmm5 ; Los paso a float
	cvtdq2ps xmm6, xmm6
	cvtdq2ps xmm7, xmm7
	cvtdq2ps xmm8, xmm8

	mulps xmm5, xmm13 ; Multiplico a todos por 1-alpha
	mulps xmm6, xmm13
	mulps xmm7, xmm13
	mulps xmm8, xmm13

	cvttps2dq xmm5, xmm5 ; Vuelvo todos a dword int con truncamiento
	cvttps2dq xmm6, xmm6
	cvttps2dq xmm7, xmm7
	cvttps2dq xmm8, xmm8

	packssdw xmm5, xmm6 ; Empaqueto todos con saturacion con signo
	packssdw xmm7, xmm8 ; Primero las cuatro dwords a words
	packuswb xmm5, xmm7 ; Y aca de words a bytes, saturando sin signo
	
	movdqu xmm4, xmm5
	
	.moverADestino:
	
	paddb xmm3, xmm4 ; En xmm0 me quedan los valores finales
		
	movdqu [rsi + r10 + 3], xmm3 ; Lo pego a partir del siguiente pixel, ya que tengo px2|px3|px4|0..0		
	sub r10, r8
	
	cmp r15, 1
	je .finLoopColumna
	
	inc r13
	add r10, 9
	cmp r13, r11
	jl .inicioLoopColumna
	sub r10, 16
	add r10, r12
	movdqu xmm0, [rdi + r10]; En xmm0 tengo 16 bytes de la fila anterior a la que voy a procesar
	psrldq xmm0, 1
	add r10, r8
	movdqu xmm1, [rdi + r10] ; En xmm1 tengo 16 bytes de la fila a procesar
	psrldq xmm1, 1
	movdqu xmm15, xmm1 ; Me lo guardo para usarlo al final
	add r10, r8
	movdqu xmm2, [rdi + r10] ; En xmm2 tengo 16 bytes de la fila siguiente a la que voy a procesar
	psrldq xmm2, 1
	sub r10, r8
	
	inc r10
	mov r15, 1
	jmp .calculoMaximos
	
	.finLoopColumna:
	inc r14 ; incremento contador de fila
	add rax, r8 ; proxima "anterior fila" es igual a actual "anterior fila" + src_row_size
	cmp r14, rdx ; si proxima fila > m - 1, termine
	jl .inicioLoopFila

	pop r15
	pop r14
	pop r13
	pop r12
	pop rbp
	ret

