; void recortar_asm (
; 	unsigned char *src,
; 	unsigned char *dst,
; 	int m,
; 	int n,
; 	int src_row_size,
; 	int dst_row_size,
; 	int tam
; );

; Parámetros:
; 	rdi = src
; 	rsi = dst
; 	edx = m     height
; 	ecx = n     width
; 	r8d = src_row_size
; 	r9d = dst_row_size
; 	ebp + 16 = tam

extern printf

section .data
formatoInteger: db '%d',0
global recortar_asm

section .text

%define src rdi
%define dst rsi
%define src_row_size r8
%define tam r9

recortar_asm:
        push rbp
        mov rbp, rsp

        sub rsp, 24 ; guardo espacio para variables

        push r12
        push r13
        push r14
        push r15
        

        mov edx, edx ; pongo ceros en la parte alta de rdx
        mov ecx, ecx ; pongo ceros en la parte alta de rcx
        mov r8d, r8d ; pongo ceros en la parte alta de r8
        mov r9d, r9d ; pongo ceros en la parte alta de r9

        mov r13, rdx ; guardo el height original
        mov r14, rcx ; guardo el width original

        mov r15, src_row_size
        mov [rbp - 8], r9 ; dst_row_size
        sub r9, [rbp + 16]
        mov r9d, r9d
        mov [rbp - 16], r9

        ;jmp .termina

        mov tam, [rbp + 16]
        mov r9d, r9d ; pongo ceros
        mov r10, rcx ; width
        mov r11, rdx; height

        mov rdx, 0
        mov rax, tam
        mov rcx, 16
        div rcx ; divido por 16
        mov rcx, rax
        mov r12, 16
        sub r12, rdx ; R12 <- resto de dividir el tamaño destino por 16

        ;esquina superior izquierda

        sub r11, tam
        mov rax, src_row_size
        mul r11 
        sub r10, tam
        add rax, r10
        mov r11, rax

        sub src_row_size, tam ; R8D <- restante
        xor r10,r10

        call chop_corner

        ;jmp .termina

        ;esquina superior derecha

        mov r10, r14 ; width
        mov r11, r13; height
        mov rdx, r13
        mov r10, tam

        sub r11, tam
        mov rax, r15
        mul r11 ; RAX <- (height - tam)* src_row_size
        mov r11, rax

        mov r10, tam
        call chop_corner

        ; esquina inferior izquierda
        
        mov r10, tam
        mov r11, r14
        sub r11, tam

        mov rax, r10
        mov r15, [rbp - 8]
        mul r15
        mov r10, rax        

        call chop_corner

        ; esquina inferior derecha

        mov rax, [rbp - 8]
        mul tam
        mov r10, rax
        add r10, tam
        xor r11,r11
        call chop_corner

.termina:
        pop r15
        pop r14
        pop r13
        pop r12

        add rsp, 24

        pop rbp
        ret


chop_corner:
        push rdi
        push rsi
        push rdx
        push rcx

        mov rdx, tam
        mov rbx, rcx
.columns:
	movdqu xmm0, [src + r11] ; cargo 16 pixels
        movdqu [dst + r10], xmm0  
        add src, 16
        add dst, 16
        cmp rcx, 0
        je .rows
	loop .columns
        ; arreglo los pixeles que me faltaban
        sub src, r12
        sub dst, r12
        mov rcx, 0
        jmp .columns
.rows:
        mov rcx, rbx; reseteo contador de columnas
        add src, src_row_size
        add rsi, [rbp - 16]

        dec rdx ; decremento contador de filas
        jnz .columns

        pop rcx
        pop rdx
        pop rsi
        pop rdi
        ret
