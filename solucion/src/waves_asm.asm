; void waves_c (
;	unsigned char *src,
;	unsigned char *dst,
;	int m,
;	int n,
;	int row_size,
;	float x_scale,
;	float y_scale,
;	float g_scale
; );

; Parámetros:
; 	rdi = src
; 	rsi = dst
; 	rdx = m
; 	rcx = n
; 	r8 = row_size
; 	xmm0 = x_scale
; 	xmm1 = y_scale
; 	xmm2 = g_scale

global waves_asm

section .text

; sin_taylor:
; 	cvtdq2ps xmm0, xmm0		;Convierto a floats
; 	
; 	mov rax, 8
; 	cvtsi2ss xmm14, eax
; 	shufps xmm14, xmm14, 0		;xmm14 = 8.0 | 8.0 | 8.0 | 8.0
; 	divps xmm0, xmm14		;xmm0 = x / 8.0
; 	mov rax, 0x40490fDB
; 	movq xmm4, rax			;xmm4 = pi
; 	shufps xmm4, xmm4, 0		;xmm4 = pi | pi | pi | pi
; 	movdqu xmm15, xmm4
; 	addps xmm4, xmm4		;xmm4 = 2pi | 2pi | 2pi | 2pi
; 	movdqu xmm14, xmm0
; 	divps xmm14, xmm4		;xmm14 = x / 2pi
; 	roundps xmm14, xmm14, 11b	;xmm14 = floor(x)
; 	mulps xmm14, xmm4		;xmm14 = floor(x / 2pi) * 2pi
; 	subps xmm0, xmm14		;xmm0 = x = x - (floor(x / 2pi) * 2pi)
; 	subps xmm0, xmm15		;xmm0 = x - pi
; 	movdqu xmm4, xmm0
; 	movdqu xmm14, xmm0
; 	mulps xmm4, xmm14
; 	mulps xmm4, xmm14		;xmm4 = x^3
; 	mov rax, 6
; 	cvtsi2ss xmm15, eax
; 	shufps xmm15, xmm15, 0		;xmm15 = 6.0 | 6.0 | 6.0 | 6.0
; 	divps xmm4, xmm15		;xmm4 = x^3 / 6.0
; 	subps xmm0, xmm4		;xmm0 = x - (x^3 / 6.0)
; 	movdqu xmm4, xmm14
; 	mulps xmm4, xmm14
; 	mulps xmm4, xmm14
; 	mulps xmm4, xmm14
; 	mulps xmm4, xmm14		;xmm4 = x^5
; 	mov rax, 120
; 	cvtsi2ss xmm15, eax
; 	shufps xmm15, xmm15, 0		;xmm15 = 120.0 | 120.0 | 120.0 | 120.0
; 	divps xmm4, xmm15		;xmm4 = x^5 / 120.0
; 	addps xmm0, xmm4		;xmm0 = x - (x^3 / 6.0) + (x^5 / 120.0)
; 	movdqu xmm4, xmm14
; 	mulps xmm4, xmm14
; 	mulps xmm4, xmm14
; 	mulps xmm4, xmm14
; 	mulps xmm4, xmm14
; 	mulps xmm4, xmm14
; 	mulps xmm4, xmm14		;xmm4 = x^7
; 	mov rax, 5040
; 	cvtsi2ss xmm15, eax
; 	shufps xmm15, xmm15, 0		;xmm15 = 5040.0 | 5040.0 | 5040.0 | 5040.0
; 	divps xmm4, xmm15		;xmm4 = x^7 / 5040.0
; 	subps xmm0, xmm4		;xmm0 = x - (x^3 / 6.0) + (x^5 / 120.0) - (x^7 / 5040.0)
; 
; 	ret

waves_asm:
	push rbp
	mov rbp, rsp
	push rbx
	push r12
	
	sub r8, rcx			;r8 = row_size - n
	mov eax, ecx
	mov ecx, edx
	xor edx, edx
	mov r9d, 16
	div r9d				;eax = n / 16, edx = resto
	mov ebx, edx			;rbx = resto
	mov edx, ecx
	mov ecx, eax
	mov r9d, ecx			;Resguardo mi contador horizontal.
	movdqu xmm3, xmm2
	shufps xmm3, xmm3, 0		;Armo una máscara de g_scale
	movdqu xmm2, xmm1
	shufps xmm2, xmm2, 0		;Armo una máscara de y_scale
	movdqu xmm1, xmm0
	shufps xmm1, xmm1, 0		;Armo una máscara de x_scale
	xor r10, r10			;Índice de filas (i)
	xor r11, r11			;Índice de columnas (j)
	mov eax, 3
	shl rax, 32
	add rax, 2
	movq xmm9, rax
	pslldq xmm9, 8
	mov rax, 0x100000000
	movq xmm0, rax
	por xmm9, xmm0			;xmm9 = 3|2|1|0
	
	.ciclo_row:
	mov ecx, r9d			;Restauro el contador horizontal.
	mov r12w, 1			;Seteo r12w distinto de cero.
	
	.ciclo_col:
	;prof = x_scale * sin_taylor(i/8) - ENMASCARADO EN UN REGISTRO.
	movq xmm0, r10
	pshufd xmm0, xmm0, 0
	
	cvtdq2ps xmm0, xmm0		;Convierto a floats
	
	mov rax, 8
	cvtsi2ss xmm14, eax
	shufps xmm14, xmm14, 0		;xmm14 = 8.0 | 8.0 | 8.0 | 8.0
	divps xmm0, xmm14		;xmm0 = x / 8.0
	mov rax, 0x40490fDB
	movq xmm4, rax			;xmm4 = pi
	shufps xmm4, xmm4, 0		;xmm4 = pi | pi | pi | pi
	movdqu xmm15, xmm4
	addps xmm4, xmm4		;xmm4 = 2pi | 2pi | 2pi | 2pi
	movdqu xmm14, xmm0
	divps xmm14, xmm4		;xmm14 = x / 2pi
	roundps xmm14, xmm14, 11b	;xmm14 = floor(x)
	mulps xmm14, xmm4		;xmm14 = floor(x / 2pi) * 2pi
	subps xmm0, xmm14		;xmm0 = x = x - (floor(x / 2pi) * 2pi)
	subps xmm0, xmm15		;xmm0 = x - pi
	movdqu xmm4, xmm0
	movdqu xmm14, xmm0
	mulps xmm4, xmm14
	mulps xmm4, xmm14		;xmm4 = x^3
	mov rax, 6
	cvtsi2ss xmm15, eax
	shufps xmm15, xmm15, 0		;xmm15 = 6.0 | 6.0 | 6.0 | 6.0
	divps xmm4, xmm15		;xmm4 = x^3 / 6.0
	subps xmm0, xmm4		;xmm0 = x - (x^3 / 6.0)
	movdqu xmm4, xmm14
	mulps xmm4, xmm14
	mulps xmm4, xmm14
	mulps xmm4, xmm14
	mulps xmm4, xmm14		;xmm4 = x^5
	mov rax, 120
	cvtsi2ss xmm15, eax
	shufps xmm15, xmm15, 0		;xmm15 = 120.0 | 120.0 | 120.0 | 120.0
	divps xmm4, xmm15		;xmm4 = x^5 / 120.0
	addps xmm0, xmm4		;xmm0 = x - (x^3 / 6.0) + (x^5 / 120.0)
	movdqu xmm4, xmm14
	mulps xmm4, xmm14
	mulps xmm4, xmm14
	mulps xmm4, xmm14
	mulps xmm4, xmm14
	mulps xmm4, xmm14
	mulps xmm4, xmm14		;xmm4 = x^7
	mov rax, 5040
	cvtsi2ss xmm15, eax
	shufps xmm15, xmm15, 0		;xmm15 = 5040.0 | 5040.0 | 5040.0 | 5040.0
	divps xmm4, xmm15		;xmm4 = x^7 / 5040.0
	subps xmm0, xmm4		;xmm0 = x - (x^3 / 6.0) + (x^5 / 120.0) - (x^7 / 5040.0)
	
	mulps xmm0, xmm1
	movdqu xmm5, xmm0
	movdqu xmm6, xmm5
	movdqu xmm7, xmm5
	movdqu xmm8, xmm5
	
	;call sin_taylor(j/8.0)
	;xmm0 = xmm0 * y_scale
	;prof1 = prof1 + xmm0
	;prof1 = prof1 * g_scale
	movq xmm0, r11
	pshufd xmm0, xmm0, 0
	paddd xmm0, xmm9
	
	cvtdq2ps xmm0, xmm0		;Convierto a floats
	
	mov rax, 8
	cvtsi2ss xmm14, eax
	shufps xmm14, xmm14, 0		;xmm14 = 8.0 | 8.0 | 8.0 | 8.0
	divps xmm0, xmm14		;xmm0 = x / 8.0
	mov rax, 0x40490fDB
	movq xmm4, rax			;xmm4 = pi
	shufps xmm4, xmm4, 0		;xmm4 = pi | pi | pi | pi
	movdqu xmm15, xmm4
	addps xmm4, xmm4		;xmm4 = 2pi | 2pi | 2pi | 2pi
	movdqu xmm14, xmm0
	divps xmm14, xmm4		;xmm14 = x / 2pi
	roundps xmm14, xmm14, 11b	;xmm14 = floor(x)
	mulps xmm14, xmm4		;xmm14 = floor(x / 2pi) * 2pi
	subps xmm0, xmm14		;xmm0 = x = x - (floor(x / 2pi) * 2pi)
	subps xmm0, xmm15		;xmm0 = x - pi
	movdqu xmm4, xmm0
	movdqu xmm14, xmm0
	mulps xmm4, xmm14
	mulps xmm4, xmm14		;xmm4 = x^3
	mov rax, 6
	cvtsi2ss xmm15, eax
	shufps xmm15, xmm15, 0		;xmm15 = 6.0 | 6.0 | 6.0 | 6.0
	divps xmm4, xmm15		;xmm4 = x^3 / 6.0
	subps xmm0, xmm4		;xmm0 = x - (x^3 / 6.0)
	movdqu xmm4, xmm14
	mulps xmm4, xmm14
	mulps xmm4, xmm14
	mulps xmm4, xmm14
	mulps xmm4, xmm14		;xmm4 = x^5
	mov rax, 120
	cvtsi2ss xmm15, eax
	shufps xmm15, xmm15, 0		;xmm15 = 120.0 | 120.0 | 120.0 | 120.0
	divps xmm4, xmm15		;xmm4 = x^5 / 120.0
	addps xmm0, xmm4		;xmm0 = x - (x^3 / 6.0) + (x^5 / 120.0)
	movdqu xmm4, xmm14
	mulps xmm4, xmm14
	mulps xmm4, xmm14
	mulps xmm4, xmm14
	mulps xmm4, xmm14
	mulps xmm4, xmm14
	mulps xmm4, xmm14		;xmm4 = x^7
	mov rax, 5040
	cvtsi2ss xmm15, eax
	shufps xmm15, xmm15, 0		;xmm15 = 5040.0 | 5040.0 | 5040.0 | 5040.0
	divps xmm4, xmm15		;xmm4 = x^7 / 5040.0
	subps xmm0, xmm4		;xmm0 = x - (x^3 / 6.0) + (x^5 / 120.0) - (x^7 / 5040.0)
	
	mulps xmm0, xmm2
	addps xmm5, xmm0
	mulps xmm5, xmm3
	
	;call sin_taylor(j+4/8.0)
	;xmm0 = xmm0 * y_scale
	;prof2 = prof2 + xmm0
	;prof2 = prof2 * g_scale
	add r11, 4
	movq xmm0, r11
	pshufd xmm0, xmm0, 0
	paddd xmm0, xmm9
	
	cvtdq2ps xmm0, xmm0		;Convierto a floats
	
	mov rax, 8
	cvtsi2ss xmm14, eax
	shufps xmm14, xmm14, 0		;xmm14 = 8.0 | 8.0 | 8.0 | 8.0
	divps xmm0, xmm14		;xmm0 = x / 8.0
	mov rax, 0x40490fDB
	movq xmm4, rax			;xmm4 = pi
	shufps xmm4, xmm4, 0		;xmm4 = pi | pi | pi | pi
	movdqu xmm15, xmm4
	addps xmm4, xmm4		;xmm4 = 2pi | 2pi | 2pi | 2pi
	movdqu xmm14, xmm0
	divps xmm14, xmm4		;xmm14 = x / 2pi
	roundps xmm14, xmm14, 11b	;xmm14 = floor(x)
	mulps xmm14, xmm4		;xmm14 = floor(x / 2pi) * 2pi
	subps xmm0, xmm14		;xmm0 = x = x - (floor(x / 2pi) * 2pi)
	subps xmm0, xmm15		;xmm0 = x - pi
	movdqu xmm4, xmm0
	movdqu xmm14, xmm0
	mulps xmm4, xmm14
	mulps xmm4, xmm14		;xmm4 = x^3
	mov rax, 6
	cvtsi2ss xmm15, eax
	shufps xmm15, xmm15, 0		;xmm15 = 6.0 | 6.0 | 6.0 | 6.0
	divps xmm4, xmm15		;xmm4 = x^3 / 6.0
	subps xmm0, xmm4		;xmm0 = x - (x^3 / 6.0)
	movdqu xmm4, xmm14
	mulps xmm4, xmm14
	mulps xmm4, xmm14
	mulps xmm4, xmm14
	mulps xmm4, xmm14		;xmm4 = x^5
	mov rax, 120
	cvtsi2ss xmm15, eax
	shufps xmm15, xmm15, 0		;xmm15 = 120.0 | 120.0 | 120.0 | 120.0
	divps xmm4, xmm15		;xmm4 = x^5 / 120.0
	addps xmm0, xmm4		;xmm0 = x - (x^3 / 6.0) + (x^5 / 120.0)
	movdqu xmm4, xmm14
	mulps xmm4, xmm14
	mulps xmm4, xmm14
	mulps xmm4, xmm14
	mulps xmm4, xmm14
	mulps xmm4, xmm14
	mulps xmm4, xmm14		;xmm4 = x^7
	mov rax, 5040
	cvtsi2ss xmm15, eax
	shufps xmm15, xmm15, 0		;xmm15 = 5040.0 | 5040.0 | 5040.0 | 5040.0
	divps xmm4, xmm15		;xmm4 = x^7 / 5040.0
	subps xmm0, xmm4		;xmm0 = x - (x^3 / 6.0) + (x^5 / 120.0) - (x^7 / 5040.0)
	
	mulps xmm0, xmm2
	addps xmm6, xmm0
	mulps xmm6, xmm3
	
	;call sin_taylor(j+8/8.0)
	;xmm0 = xmm0 * y_scale
	;prof3 = prof3 + xmm0
	;prof3 = prof3 * g_scale
	add r11, 4
	movq xmm0, r11
	pshufd xmm0, xmm0, 0
	paddd xmm0, xmm9
	
	cvtdq2ps xmm0, xmm0		;Convierto a floats
	
	mov rax, 8
	cvtsi2ss xmm14, eax
	shufps xmm14, xmm14, 0		;xmm14 = 8.0 | 8.0 | 8.0 | 8.0
	divps xmm0, xmm14		;xmm0 = x / 8.0
	mov rax, 0x40490fDB
	movq xmm4, rax			;xmm4 = pi
	shufps xmm4, xmm4, 0		;xmm4 = pi | pi | pi | pi
	movdqu xmm15, xmm4
	addps xmm4, xmm4		;xmm4 = 2pi | 2pi | 2pi | 2pi
	movdqu xmm14, xmm0
	divps xmm14, xmm4		;xmm14 = x / 2pi
	roundps xmm14, xmm14, 11b	;xmm14 = floor(x)
	mulps xmm14, xmm4		;xmm14 = floor(x / 2pi) * 2pi
	subps xmm0, xmm14		;xmm0 = x = x - (floor(x / 2pi) * 2pi)
	subps xmm0, xmm15		;xmm0 = x - pi
	movdqu xmm4, xmm0
	movdqu xmm14, xmm0
	mulps xmm4, xmm14
	mulps xmm4, xmm14		;xmm4 = x^3
	mov rax, 6
	cvtsi2ss xmm15, eax
	shufps xmm15, xmm15, 0		;xmm15 = 6.0 | 6.0 | 6.0 | 6.0
	divps xmm4, xmm15		;xmm4 = x^3 / 6.0
	subps xmm0, xmm4		;xmm0 = x - (x^3 / 6.0)
	movdqu xmm4, xmm14
	mulps xmm4, xmm14
	mulps xmm4, xmm14
	mulps xmm4, xmm14
	mulps xmm4, xmm14		;xmm4 = x^5
	mov rax, 120
	cvtsi2ss xmm15, eax
	shufps xmm15, xmm15, 0		;xmm15 = 120.0 | 120.0 | 120.0 | 120.0
	divps xmm4, xmm15		;xmm4 = x^5 / 120.0
	addps xmm0, xmm4		;xmm0 = x - (x^3 / 6.0) + (x^5 / 120.0)
	movdqu xmm4, xmm14
	mulps xmm4, xmm14
	mulps xmm4, xmm14
	mulps xmm4, xmm14
	mulps xmm4, xmm14
	mulps xmm4, xmm14
	mulps xmm4, xmm14		;xmm4 = x^7
	mov rax, 5040
	cvtsi2ss xmm15, eax
	shufps xmm15, xmm15, 0		;xmm15 = 5040.0 | 5040.0 | 5040.0 | 5040.0
	divps xmm4, xmm15		;xmm4 = x^7 / 5040.0
	subps xmm0, xmm4		;xmm0 = x - (x^3 / 6.0) + (x^5 / 120.0) - (x^7 / 5040.0)
	
	mulps xmm0, xmm2
	addps xmm7, xmm0
	mulps xmm7, xmm3
	
	;call sin_taylor(j+12/8.0)
	;xmm0 = xmm0 * y_scale
	;prof4 = prof4 + xmm0
	;prof4 = prof4 * g_scale
	add r11, 4
	movq xmm0, r11
	pshufd xmm0, xmm0, 0
	paddd xmm0, xmm9
	
	cvtdq2ps xmm0, xmm0		;Convierto a floats
	
	mov rax, 8
	cvtsi2ss xmm14, eax
	shufps xmm14, xmm14, 0		;xmm14 = 8.0 | 8.0 | 8.0 | 8.0
	divps xmm0, xmm14		;xmm0 = x / 8.0
	mov rax, 0x40490fDB
	movq xmm4, rax			;xmm4 = pi
	shufps xmm4, xmm4, 0		;xmm4 = pi | pi | pi | pi
	movdqu xmm15, xmm4
	addps xmm4, xmm4		;xmm4 = 2pi | 2pi | 2pi | 2pi
	movdqu xmm14, xmm0
	divps xmm14, xmm4		;xmm14 = x / 2pi
	roundps xmm14, xmm14, 11b	;xmm14 = floor(x)
	mulps xmm14, xmm4		;xmm14 = floor(x / 2pi) * 2pi
	subps xmm0, xmm14		;xmm0 = x = x - (floor(x / 2pi) * 2pi)
	subps xmm0, xmm15		;xmm0 = x - pi
	movdqu xmm4, xmm0
	movdqu xmm14, xmm0
	mulps xmm4, xmm14
	mulps xmm4, xmm14		;xmm4 = x^3
	mov rax, 6
	cvtsi2ss xmm15, eax
	shufps xmm15, xmm15, 0		;xmm15 = 6.0 | 6.0 | 6.0 | 6.0
	divps xmm4, xmm15		;xmm4 = x^3 / 6.0
	subps xmm0, xmm4		;xmm0 = x - (x^3 / 6.0)
	movdqu xmm4, xmm14
	mulps xmm4, xmm14
	mulps xmm4, xmm14
	mulps xmm4, xmm14
	mulps xmm4, xmm14		;xmm4 = x^5
	mov rax, 120
	cvtsi2ss xmm15, eax
	shufps xmm15, xmm15, 0		;xmm15 = 120.0 | 120.0 | 120.0 | 120.0
	divps xmm4, xmm15		;xmm4 = x^5 / 120.0
	addps xmm0, xmm4		;xmm0 = x - (x^3 / 6.0) + (x^5 / 120.0)
	movdqu xmm4, xmm14
	mulps xmm4, xmm14
	mulps xmm4, xmm14
	mulps xmm4, xmm14
	mulps xmm4, xmm14
	mulps xmm4, xmm14
	mulps xmm4, xmm14		;xmm4 = x^7
	mov rax, 5040
	cvtsi2ss xmm15, eax
	shufps xmm15, xmm15, 0		;xmm15 = 5040.0 | 5040.0 | 5040.0 | 5040.0
	divps xmm4, xmm15		;xmm4 = x^7 / 5040.0
	subps xmm0, xmm4		;xmm0 = x - (x^3 / 6.0) + (x^5 / 120.0) - (x^7 / 5040.0)
	
	mulps xmm0, xmm2
	addps xmm8, xmm0
	mulps xmm8, xmm3
	
	cvtps2dq xmm5, xmm5
	cvtps2dq xmm6, xmm6
	cvtps2dq xmm7, xmm7
	cvtps2dq xmm8, xmm8
	;prof = prof / 2
	psrad xmm5, 1
	psrad xmm6, 1
	psrad xmm7, 1
	psrad xmm8, 1
	movdqu xmm10, [rdi]
	movdqu xmm11, xmm10
	pxor xmm15, xmm15		;Voy a usarlo para desempaquetar con ceros.
	punpcklbw xmm10, xmm15
	punpckhbw xmm11, xmm15
	movdqu xmm12, xmm10
	movdqu xmm13, xmm11
	punpcklwd xmm10, xmm15
	punpckhwd xmm12, xmm15
	punpcklwd xmm11, xmm15
	punpckhwd xmm13, xmm15
	;dest = prof + orig
	paddd xmm10, xmm5
	paddd xmm12, xmm6
	paddd xmm11, xmm7
	paddd xmm13, xmm8
	packusdw xmm10, xmm12
	packusdw xmm11, xmm13
	packuswb xmm10, xmm11
	
	movdqu [rsi], xmm10		;Cargo al destino los pixels procesados.
	
	add rdi, 16
	add rsi, 16
	add r11, 4
	dec ecx
	jnz .ciclo_col
	
	cmp r12w, 0			;Si ya procesé hasta el final, sigo.
	je .sigue_ciclo
	
	cmp ebx, 0
	je .sigue_ciclo
	add rdi, rbx
	sub rdi, 16			;Me posiciono para poder procesar los últimos pixels
	add rsi, rbx
	sub rsi, 16
	add r11, rbx
	sub r11, 16
	inc ecx
	xor r12w, r12w			;Indico que se procesó hasta el final.
	jmp .ciclo_col
	
	.sigue_ciclo:
	xor r11, r11
	inc r10
	add rdi, r8
	add rsi, r8
	dec edx
	jnz .ciclo_row

	pop r12
	pop rbx
	pop rbp
	ret
