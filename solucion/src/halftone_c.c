void halftone_c (
	unsigned char *src,
	unsigned char *dst,
	int m,
	int n,
	int src_row_size,
	int dst_row_size
) {
	unsigned char (*src_matrix)[src_row_size] = (unsigned char (*)[src_row_size]) src;
	unsigned char (*dst_matrix)[dst_row_size] = (unsigned char (*)[dst_row_size]) dst;

	int i = 0, suma;
	while(i < m - (m % 2)) {
		int j = 0;
		while(j < n - (n % 2)) {
			//Sumo sus valores
			suma = src_matrix[i][j] + src_matrix[i][j+1] + src_matrix[i+1][j] + src_matrix[i+1][j+1];
			if(suma >= 820){
				//Si es >= 820 -> seteo 255 en todos.
				dst_matrix[i][j] = 255;
				dst_matrix[i][j+1] = 255;
				dst_matrix[i+1][j] = 255;
				dst_matrix[i+1][j+1] = 255;
			}
			else if(suma >= 615) {
				//Si es >= 615 -> seteo 255 en una L y 0 en el segundo de la primera fila
				dst_matrix[i][j] = 255;
				dst_matrix[i][j+1] = 0;
				dst_matrix[i+1][j] = 255;
				dst_matrix[i+1][j+1] = 255;
			}
			else if(suma >= 410) {
				//Si es >= 410 -> seteo 255 en el primero y el último, y 0 en el segundo y el 3ero
				dst_matrix[i][j] = 255;
				dst_matrix[i][j+1] = 0;
				dst_matrix[i+1][j] = 0;
				dst_matrix[i+1][j+1] = 255;
			}
			else if(suma >= 205) {
				//Si es >= 205 -> seteo 255 en el primero y el resto en 0
				dst_matrix[i][j] = 255;
				dst_matrix[i][j+1] = 0;
				dst_matrix[i+1][j] = 0;
				dst_matrix[i+1][j+1] = 0;
			}
			else {
				//ELSE	       -> seteo todo en 0
				dst_matrix[i][j] = 0;
				dst_matrix[i][j+1] = 0;
				dst_matrix[i+1][j] = 0;
				dst_matrix[i+1][j+1] = 0;
			}
			//Preparo el siguiente bloque para iterar.
			j = j + 2;
		}
		i = i + 2;
	}	

}
